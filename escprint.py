#!/usr/bin/env python3

import argparse
import configparser
import os.path
import sys
import win32print
import struct

ESC = b'\x1B'
GS = b'\x1D'
RESET = ESC + b'@'
LINEFEED = ESC + b'd'
CODEPAGE = ESC + b't'

CODEPAGE_IDS = {
	'cp437': 0,
	'cp850': 2,
	'cp1252': 16
}

script_dir = os.path.dirname(__file__)
config_file = os.path.join(script_dir, 'settings.ini')
config = configparser.ConfigParser()

if os.path.exists(config_file):
	config.read(config_file)

parser = argparse.ArgumentParser(description='Prints a document using ESC/POS')
parser.add_argument('--printer', help='Printer to use.', default=config.get('settings', 'printer', fallback=None))
parser.add_argument('--charset', help='Charset encoding. Default is CP437.', default=config.get('settings', 'charset', fallback='cp437'))
parser.add_argument('--feedafter', help='Number of lines to feed after printing the document. Default is none.', default=config.get('settings', 'feedafter', fallback=0), type=int)
parser.add_argument('file', help='File to print. If not specified, it will be read from stdin.', nargs='?', default=sys.stdin, type=argparse.FileType('r', encoding='utf-8'))
args = parser.parse_args()

if not args.printer:
	print('No printer name was specified and no default is set', file=sys.stderr)
	sys.exit(1)

cp_id = CODEPAGE_IDS.get(args.charset.lower())
if cp_id is None:
	print('Unsupported charset "%s"' % args.charset)
	sys.exit(1)

text = args.file.read()
print(text)

try:
	text = text.encode(args.charset)
except UnicodeError:
	print('Unable to encode text to target encoding.')
	sys.exit(1)

p = win32print.OpenPrinter(args.printer)
win32print.StartDocPrinter(p, 1, ('Line document', None, 'raw'))
win32print.WritePrinter(p, RESET)
win32print.WritePrinter(p, CODEPAGE + struct.pack('B', cp_id))
win32print.WritePrinter(p, text)
if args.feedafter > 0:
	win32print.WritePrinter(p, LINEFEED + struct.pack('B', args.feedafter))
win32print.EndDocPrinter(p)
win32print.ClosePrinter(p)
